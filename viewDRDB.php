<?php
// Author: Troy Braswell, July 18, 2018

// Set Server Info
include('config.php');

// Set Logging
ini_set("error_log","C:\Apache24\logs\midPlane_error.log");
ini_set("display_errors","Off");

// Get Args
$q = $_POST['str'];
$search_type = explode ("x|x",$q)[0];

if ($search_type == 'date'){
	$startDate = explode ("x|x",$q)[1];
	$endDate = explode ("x|x",$q)[2];
	
	// Offset End Date by 1 day for search
	$endDate2 = date("Y-m-d", strtotime($endDate. ' + 1 days'));
	// Create DATES
	$begin = new DateTime($startDate);
	$end = new DateTime($endDate2 );
	$interval = DateInterval::createFromDateString('1 day');
	$period = new DatePeriod($begin, $interval, $end);
	
	// Prepare Statement for Date Range Search
	$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
		FROM $tableName_inspections m
		LEFT JOIN $tableName_utds u ON m.serial = u.serial
		WHERE m.date>='$startDate'
		AND m.date<='$endDate2'
		ORDER BY m.date ASC";
		
	echo "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections\n$startDate / $endDate</h2>";
	echo "<p><a href='exportDRDB.php?arg1=$search_type&arg2=$startDate&arg3=$endDate2'>Export Results</a></p>";
	
} else if ($search_type == 'date_today'){
	$startDate = explode ("x|x",$q)[1];
	$endDate = explode ("x|x",$q)[2];
	$file = "MidPlane-Inspection_".$startDate."_".$endDate.".csv";
	// Prepare Statement	
	$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
		FROM $tableName_inspections m
		LEFT JOIN $tableName_utds u ON m.serial = u.serial
		WHERE m.date>='$startDate'
		AND m.date<='$endDate'
		ORDER BY m.date ASC";
		
	echo "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections\n$date</h2>";
	echo "<p><a href='exportDRDB.php?arg1=$search_type&arg2=$date&arg3=$endDate'>Export Results</a></p>";

} else if ($search_type == 'date2'){
	$startDate = explode ("x|x",$q)[1];
	$endDate = explode ("x|x",$q)[2];
	$file = "MidPlane-Inspection_".$startDate."_".$endDate.".csv";
	// Prepare Statement
	$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
		FROM $tableName_inspections m
		LEFT JOIN $tableName_utds u ON m.serial = u.serial
		WHERE m.date>='$startDate'
		AND m.date<='$endDate'
		ORDER BY m.date ASC";
		
	echo "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections\n$startDate</h2>";
	echo "<p><a href='exportDRDB.php?arg1=$search_type&arg2=$startDate&arg3=$endDate'>Export Results</a></p>";
} else if ($search_type == 'asset') {
	$asset = explode ("x|x",$q)[1];
	
	// Prepare Statement for Asset Tag Search		
	$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
		FROM $tableName_inspections m
		LEFT JOIN $tableName_utds u ON m.serial = u.serial
		WHERE m.asset_tag ='$asset'";
	echo "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections for\n$asset</h2>";
	echo "<p><a href='exportDRDB.php?arg1=$search_type&arg2=$asset'>Export Results</a></p>";
	
} else if ($search_type == 'sql-30days-plus'){
	$name = explode ("x|x",$q)[1];
	$thirtyDaysAgo = explode ("x|x",$q)[2];
	$file = "MidPlane-Inspection_".$name.".csv";
	$sql = "SELECT id,serial, assy_num, asset_tag, mac_addr,user,status,cleaned,comments,date,badLC,badFM 
	FROM $tableName_inspections 
	WHERE serial NOT IN 
	(SELECT serial 
	FROM $tableName_inspections 
	WHERE date>='$thirtyDaysAgo' 
	GROUP BY(serial)) 
	GROUP BY(serial)";
	error_log($sql, 0);
} else if ($search_type == 'sql-no_insp'){
	$name = explode ("x|x",$q)[1];
	$endDate = explode ("x|x",$q)[2];
	$file = "MidPlane-Inspection_".$name.".csv";
	$sql = "SELECT id,serial,assy_num,product_name,product,asset_tag
	FROM $tableName_utds 
	WHERE serial NOT IN 
	(SELECT serial 
	FROM $tableName_inspections 
	WHERE date>='$endDate' 
	GROUP BY(serial)) 
	GROUP BY(serial)";
	error_log($sql, 0);
}



// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// Execute Statement
$result_info = $conn->query($sql);

// Process results into Array, if results were found
if ($result_info->num_rows > 0) {

	$darkColor = '#00b3b3';
	$lightColor = '#FFFFDB';
	$failColor = '#ed2d1c';
	// If displaying fixtures that have never been inspected.  We are looking at a different table than
	// 	normal so the headers and data will be different
	if ($search_type == 'sql-no_insp'){
		echo "<table style='font-size: medium;'>
		<tr style='background-color: $darkColor;'>
		<th>Serial</th>
		<th>Assembly Number</th>
		<th>Product Name</th>
		<th>Product Description</th>
		<th>Asset Tag</th>
		</tr>";
		
		$color_switch=0;

		while($row = $result_info->fetch_assoc()) {
			
			if ($color_switch == 0) {
				echo "<tr style='background-color: $lightColor;'><td style='text-align: center;border: 1px solid black ;'>" . $row['serial'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['assy_num'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['product_name'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['product'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['asset_tag'] . "</td>";
			} else {
				echo "<tr style='background-color: $lightColor;'><td style='text-align: center;border: 1px solid black ;'>" . $row['serial'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['assy_num'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['product_name'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['product'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['asset_tag'] . "</td>";
			}
		
			echo "</tr>";
			if ($color_switch == 0) {
				$color_switch += 1;
			} else {
				$color_switch -= 1;
			}
			
		}

	} else {
		echo "<table style='font-size: small;'>
		<tr style='background-color: $darkColor;'>
		<th>#</th>
		<th>Date</th>
		<th>Status</th>
		<th>Cleaned</th>
		<th>Serial</th>
		<th>Product</th>
		<th>Assembly Number</th>
		<th>Asset Tag</th>
		<th>Mac Address</th>
		<th>Inspector</th>
		<th># Bad LC</th>
		<th># Bad FM</th>
		<th>Comments</th>
		<th>Extra</th>
		</tr>";
	
	
		$color_switch=0;

		while($row = $result_info->fetch_assoc()) {

			$funcString = "'" . $row['date'] . "','" . $row['asset_tag'] . "'"; 
			$mydate= (string)$row['date'];
			$id = (string)$row['id'];
			$myasset = $row['asset_tag'];
			if ($color_switch == 0) {
				
				echo "<tr style='background-color: $lightColor;'><td style='text-align: center;border: 1px solid black ;'>" . $row['id'] . "</td><td style='text-align: center;border: 1px solid black ;'>" . $row['date'] . "</td>";
				if ($row['status'] == 'FAIL'){
					echo "<td style='text-align: center;border: 1px solid black ;background-color: $failColor;'><a href='javascript:void(0)' onclick='viewInspection(&#39;$mydate&#39;&#44;&#39;$myasset&#39;)'>" . $row['status'] . "</a></td>";
				} else {
					echo "<td style='text-align: center;border: 1px solid black ;'><a href='javascript:void(0)' onclick='viewInspection(&#39;$mydate&#39;&#44;&#39;$myasset&#39;)'>" . $row['status'] . "</a></td>";
				}
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['cleaned'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['serial'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['product'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['assy_num'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['asset_tag'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['mac_addr'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['user'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['badLC'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['badFM'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['comments'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'><a id='delroot' class='hideme' href='#DeleteDone' onclick='deleteEntry(\"$id\",\"$q\")'>Delete</a></td>";
			} else {
				echo "<tr style='background-color: $darkColor;'><td style='text-align: center;border: 1px solid black ;'>" . $row['id'] . "</td><td style='text-align: center;border: 1px solid black ;'>" . $row['date'] . "</td>";
				
				if ($row['status'] == 'FAIL'){
					echo "<td style='text-align: center;border: 1px solid black ;background-color: $failColor;'><a href='javascript:void(0)' onclick='viewInspection(&#39;$mydate&#39;&#44;&#39;$myasset&#39;)'>" . $row['status'] . "</a></td>";
				} else {
					echo "<td style='text-align: center;border: 1px solid black ;'><a href='javascript:void(0)' onclick='viewInspection(&#39;$mydate&#39;&#44;&#39;$myasset&#39;)'>" . $row['status'] . "</a></td>";
				}
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['cleaned'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['serial'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['product'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['assy_num'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['asset_tag'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['mac_addr'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['user'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['badLC'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['badFM'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'>" . $row['comments'] . "</td>";
				echo "<td style='text-align: center;border: 1px solid black ;'><a id='delroot' class='hideme' href='#DeleteDone' onclick='deleteEntry(\"$id\",\"$q\")'>Delete</a></td>";
			}
				
			echo "</tr>";
			if ($color_switch == 0) {
				$color_switch += 1;
			} else {
				$color_switch -= 1;
			}

		}
	}
	echo "</table>";

} else {
	// If no Results were found
	echo "<br><b>No Results found in DB</b>";
}
// Close Connection
$conn->close();
?>