@echo off
:: This batch file backs up the database to the cloud.  It is set in windows scheduler to execute everytime the user logs in and every half hour after
:: make sure to change the settings from line 4-9
set dbUser=fake_user
set dbPassword="fake_password"
set mysqldump="C:\Program Files\MySQL\MySQL Server 5.7\bin\mysqldump.exe"
set zip="C:\Program Files\7-Zip\7z.exe"
set TIMESTAMP=%DATE:~10,4%%DATE:~4,2%%DATE:~7,2%

"C:/Program Files/MySQL/MySQL Server 5.7/bin/mysqldump.exe" -uroot -pDebug_Me arista_tooling_inspection > C:\Apache24\htdocs\TestFixture_dailys\backups\midplane_bak.%TIMESTAMP%.sql
%zip% a -tzip C:\Apache24\htdocs\TestFixture_dailys\backups\midplane_bak.%TIMESTAMP%.zip C:\Apache24\htdocs\TestFixture_dailys\uploads
%zip% a -tzip C:\Apache24\htdocs\TestFixture_dailys\backups\midplane_bak.%TIMESTAMP%.zip C:\Apache24\htdocs\TestFixture_dailys\backups\midplane_bak.%TIMESTAMP%.sql
del C:\Apache24\htdocs\TestFixture_dailys\backups\midplane_bak.%TIMESTAMP%.sql

xcopy /y "C:\Apache24\htdocs\TestFixture_dailys\backups\midplane_bak.%TIMESTAMP%.zip" "S:\path\to\cloud\backup\folder"
