This project was developed by me to allow for our test operators to log inspections of test racks and chassis.  The logs and any pictures are uploaded to a local MySQL database.

<h1>Main Page</h1>
![alt text](readme_files/main_page.JPG "Main Page")

<h1>Inspection List</h1>

![alt text](readme_files/inspection_list.png.jpg "Inspection List")