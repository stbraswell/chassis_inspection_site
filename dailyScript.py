import requests
import sys
import threading
import collections
import datetime
import re
import os
import csv
import mysql.connector
import traceback
from mysql.connector import errorcode
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders




dir = os.path.dirname(__file__)
if not dir:
	dir = os.getcwd()

date_yesterday = str(datetime.datetime.now().date()- datetime.timedelta(days=1))

outFile = os.path.join(dir,'Daily_CI_Report-%s.csv') %date_yesterday
user_dir = os.environ['USERPROFILE']

DB_NAME = 'arista_tooling_inspection'
tableName = 'utd_list'
config = {
  'user': 'fake_user',
  'password': 'fake_password',
  'host': '127.0.0.1',
  'raise_on_warnings': False,
}


emailHTML='<html><body>\n\
		Hi All,<br>\n\
		<br>\n\
		<div>\n\
		<p>Please see attached file for yesterday\'s Chassis Inspection results.<br></p>\n\
		Thanks,<br>\n\
		User1<br>\n\
		</body></html>' 

emailHTML_nada='<html><body>\n\
		Hi All,<br>\n\
		<br>\n\
		<div>\n\
		<p>There were no Chassis Inspections performed yesterday.<br></p>\n\
		Thanks,<br>\n\
		User1<br>\n\
		</body></html>'
def emailit(body):
	d = datetime.datetime.now()
	at_datetime = d.strftime("%m-%d-%Y @ %I.%M %p")
	fromaddr = "user1@company.com"
	toaddr = ["user2@company.com",
				"user3@company.com",
				"user4@company.com",
				"user5@company.com"]
	pfile = os.path.join(user_dir,'pfile.txt')	
	pfile2 = os.path.join(user_dir,'pfile2.txt')
	msg = MIMEMultipart('alternative')
	subject= 'Daily Chassis Inspection Report for %s' % date_yesterday
	msg['From'] = fromaddr
	msg['To'] = ", ".join(toaddr) #toaddr
	msg['Subject'] = subject
	file = open(pfile, 'rb') 
	pword= file.read() 
	file.close()
	file = open(pfile2, 'rb') 
	pword2= file.read() 
	file.close()
	text = "You found Waldo!  Great Job! -User1"
	part1 = MIMEText(text, 'plain')
	part2 = MIMEText(body, 'html')
	msg.attach(part1)
	msg.attach(part2)
	
	# Attatchment 1
	if os.path.isfile(outFile):
		filename = outFile.split('\\')[-1]
		attachment = open(outFile, "rb")
		part = MIMEBase('application', 'octet-stream')
		part.set_payload((attachment).read())
		encoders.encode_base64(part)
		part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
		msg.attach(part)
	
	#Send it
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	try:
		server.login(fromaddr, pword)
	except:
		print 'Using pfile2'
		server.login(fromaddr, pword2)
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()




# connect to DB
try:
	cnx = mysql.connector.connect(**config)

except mysql.connector.Error as err:
	if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
		print("Something is wrong with your user name or password")
	elif err.errno == errorcode.ER_BAD_DB_ERROR:
		print("Database does not exist")
	else:
		print(err)


cursor = cnx.cursor()


try:
	cnx.database = DB_NAME  
except mysql.connector.Error as err:
	if err.errno == errorcode.ER_BAD_DB_ERROR:
		create_database(cursor)
		cnx.database = DB_NAME
	else:
		print(err)
		exit(1)

endDate = datetime.datetime.now()
endDate = endDate.replace(minute=0, hour=0, second=0, microsecond=0)
startDate = endDate - datetime.timedelta(days=1)


query = ("""SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
		FROM midplane_inspection m
		LEFT JOIN utd_list u ON m.serial = u.serial
		WHERE m.date>=%s
		AND m.date<=%s
		ORDER BY m.date ASC""") 
		
cursor.execute(query,(startDate,endDate))		
dataHold = cursor.fetchall()
if len(dataHold) > 0:
	with open(outFile, 'wb') as f:
		clusterwriter = csv.writer(f)
		clusterwriter.writerow(['Id','Serial Number','Assembly Number','Asset Tag','Mac Address','User','Status','Cleaned','Comments','Date','Number of Bad FM slots','Number of Bad LC slots', 'Product'])
		for item in dataHold:
			clusterwriter.writerow(item)
	emailit(emailHTML)
else:
	emailit(emailHTML_nada)
		
cursor.close()
cnx.close()