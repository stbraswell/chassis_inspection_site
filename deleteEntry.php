<?php
// Author: Troy Braswell, July 19, 2018

 // Set Logging
ini_set("error_log","C:\Apache24\logs\midPlane_error.log");
ini_set("display_errors","Off");
// Set Vars
include('config.php');

$id = $_GET['arg1'];

$sql = "DELETE FROM midplane_inspection WHERE id = '$id'";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
		
// Execute Statement
$result = $conn->query($sql);
error_log("Delete result: $result");

//Close Connection
$conn->close(); 
?>
