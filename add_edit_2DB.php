<?php
// Author: Troy Braswell, March 13, 2018
// Set Logging
ini_set("error_log","C:\Apache24\logs\midPlane_error.log");
ini_set("display_errors","Off");
// Grab data passed from html
$q = $_POST['str'];

if (isset($_FILES["files"])) {
	$numberOfFiles = sizeof($_FILES["files"]["name"]);
} else {
	$numberOfFiles = 0;
}
error_log("numberof files: " . $numberOfFiles, 0);
$uploadOk = 1;

// Setup Vars
include('config.php');
$data = array();
$tooManyCleanings = 0;
$countOfSns = 0;

//remove single and double quotes from app input except for comments.  For comments, escape the quotes
// MAKE SURE YOU 'ADDSLASHES' WHEN YOU READ THE DB!!!

$user = str_replace(array('"', "'"), '', explode ("x|x",$q)[0]);
$arAsset = str_replace(array('"', "'"), '', explode ("x|x",$q)[1]);
$macAddress = str_replace(array('"', "'"), '', explode ("x|x",$q)[2]);
$assyNum = str_replace(array('"', "'"), '', explode ("x|x",$q)[3]);
$serialNum = str_replace(array('"', "'"), '', explode ("x|x",$q)[4]);
$comment = addslashes(explode ("x|x",$q)[5]);
$passfail = explode ("x|x",$q)[6];
$date_org = explode ("x|x",$q)[7];
$cleaned = explode ("x|x",$q)[8];
$id		= explode ("x|x",$q)[9];
$badFMs		= explode ("x|x",$q)[10];
$badLCs		= explode ("x|x",$q)[11];
$date = date('Y-m-d_H', strtotime($date_org));


$foldername = $_SERVER['DOCUMENT_ROOT'] . "/" . $target_dir . $arAsset . "_" . $date;

if (!file_exists($foldername)) {
    mkdir($foldername,0777, true);
}

for ($x = 0; $x < $numberOfFiles; $x++) {
	$fileName = $_FILES["files"]["name"][$x];
	$tmp_name = $_FILES["files"]["tmp_name"][$x];
	$fileArr[$fileName] = $tmp_name;
	$target_file = $foldername . "/" . basename($_FILES["files"]["name"][$x]);
	$server_target_file = $serverURL . "/" . $target_dir . $arAsset . "_" . $date . "/" . basename($_FILES["files"]["name"][$x]);
	
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	
	// Check if file already exists
	if (file_exists($target_file)) {
		echo "Sorry, file already exists.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["files"]["size"][$x] > 500000) {
		echo "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
		echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
	}
	
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["files"]["tmp_name"][$x], $target_file)) {
			// Store file paths in an array
			$realFileArr[] = $server_target_file;
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
	}
	
}

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


// Prepare DB Create Statement
$sql = "CREATE DATABASE IF NOT EXISTS ar_tooling_inspection;";

// Check for DB, if it doesn't exist... Create it
if ($conn->query($sql) === TRUE) {
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname );
} else {
    echo "Error creating database: " . $conn->error;
}
if (isset($realFileArr)) {
	// Prepare Statement to get previous files
	$sql = "SELECT pic_path 
	FROM $tableName_inspections
	WHERE serial ='$serialNum'
	AND date='$date_org'";
	error_log($sql, 0);
	// Execute Statement
	$result_info = $conn->query($sql);

	if ($result_info->num_rows > 0) {
		while($row = $result_info->fetch_assoc()) {
			print_r($row);
			$oldFiles=$row['pic_path'];
		}
		
	}
			
	$mySQLfiles = ''.implode('|',$realFileArr);		
	if (isset($oldFiles)) {
		echo "addmtogether";
		$AllMySQLfiles = $oldFiles . "|" . $mySQLfiles;
	}
} else {
	$AllMySQLfiles = '';
}

if (empty($AllMySQLfiles)) {
	$sql = "UPDATE $tableName_inspections 
			SET user = '$user', serial = '$serialNum',
			asset_tag = '$arAsset', mac_addr = '$macAddress',
			assy_num = '$assyNum', status = '$passfail',
			comments = '$comment', cleaned = '$cleaned',
			badFM = '$badFMs', badLC = '$badLCs'
			WHERE id='$id'";		
			
} else {
	$sql = "UPDATE $tableName_inspections 
			SET user = '$user', serial = '$serialNum',
			asset_tag = '$arAsset', mac_addr = '$macAddress',
			assy_num = '$assyNum', pic_path='$AllMySQLfiles',
			comments = '$comment', cleaned = '$cleaned',
			badFM = '$badFMs', badLC = '$badLCs',status = '$passfail'
			WHERE id='$id'";
}
error_log($sql, 0);

echo "r|r";
// Execute Statement
$result_info = $conn->query($sql);
echo "Added log of $arAsset as $passfail to DB";

// Close Connection
$conn->close();
?>