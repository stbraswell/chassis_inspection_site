"""
Scheduler to run 'dailyScript.py', which sends out email to managers detailing the previous days inspection activity
"""

from datetime import datetime
import logging
import apscheduler
import os
from apscheduler.schedulers.blocking import BlockingScheduler

class ContextFilter(logging.Filter):
    # This is a filter which injects contextual information into the log.
	
	compName = os.environ['COMPUTERNAME']

	def filter(self, record):
		record.cName = ContextFilter.compName
		return True
logger = logging.getLogger('apscheduler.scheduler')
logger_exec = logging.getLogger('apscheduler.executors.default')
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-30s %(cName)s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='scheduler_daily.log')

f = ContextFilter()
logger.addFilter(f)
logger_exec.addFilter(f)

	
def getYesterdayActivity():
	print('Tick! The time is: %s  | Running daily_breakdown' % datetime.now())
	os.system("C:/path/to/daily/script/dailyScript.py")
	
	
if __name__ == '__main__':
	scheduler = BlockingScheduler()
	scheduler.add_job(getYesterdayActivity, 'interval', hours=24, start_date='2019-02-13 01:30:00', id='yes_activity')

	print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

	try:
		scheduler.start()
	except (KeyboardInterrupt, SystemExit):
		pass