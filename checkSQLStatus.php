<?php
// Author: Troy Braswell, March 12, 2018

include('config.php');

$sqlResult = '';
// Set Logging
ini_set("error_log","C:\Apache24\logs\Loopback_Tracker.log");
ini_set("display_errors","Off");
error_reporting(E_ERROR | E_PARSE);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    $sqlResult = "assets/redlight.png";
} else {
	$sqlResult = "assets/greenlight.png";
}

// Close Connection
$conn->close();
echo $sqlResult;