<?php
// Author: Troy Braswell, March 13, 2018

// Grab data passed from html
$q = $_POST['str'];

if (isset($_FILES["files"])) {
	$numberOfFiles = sizeof($_FILES["files"]["name"]);
} else {
	$numberOfFiles = 0;
}
$uploadOk = 1;

// Setup Vars
include('config.php');
$data = array();
$tooManyCleanings = 0;
$countOfSns = 0;

// Set Logging
ini_set("error_log","C:\Apache24\logs\midPlane_error.log");
ini_set("display_errors","Off");

//remove single and double quotes from app input except for comments.  For comments, escape the quotes
// MAKE SURE YOU 'ADDSLASHES' WHEN YOU READ THE DB!!!

$user = str_replace(array('"', "'"), '', explode ("x|x",$q)[0]);
$arAsset = str_replace(array('"', "'"), '', explode ("x|x",$q)[1]);
$macAddress = str_replace(array('"', "'"), '', explode ("x|x",$q)[2]);
$assyNum = str_replace(array('"', "'"), '', explode ("x|x",$q)[3]);
$serialNum = str_replace(array('"', "'"), '', explode ("x|x",$q)[4]);
$comment = addslashes(explode ("x|x",$q)[5]);
$passfail = explode ("x|x",$q)[6];
$cleaned = explode ("x|x",$q)[7];
$badFMs = explode ("x|x",$q)[8];
$badLCs = explode ("x|x",$q)[9];
$date = date('Y-m-d_H');
echo $cleaned;

$foldername = $_SERVER['DOCUMENT_ROOT'] . "/" . $target_dir . $arAsset . "_" . $date;

if (!file_exists($foldername)) {
    mkdir($foldername,0777, true);
}

for ($x = 0; $x < $numberOfFiles; $x++) {
	$fileName = $_FILES["files"]["name"][$x];
	$tmp_name = $_FILES["files"]["tmp_name"][$x];
	$fileArr[$fileName] = $tmp_name;
	$target_file = $foldername . "/" . basename($_FILES["files"]["name"][$x]);
	$server_target_file = $serverURL . "/" . $target_dir . $arAsset . "_" . $date . "/" . basename($_FILES["files"]["name"][$x]);
	
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	
	// Check if file already exists
	if (file_exists($target_file)) {
		echo "Sorry, file already exists.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["files"]["size"][$x] > 500000) {
		echo "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
		echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
	}
	
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["files"]["tmp_name"][$x], $target_file)) {
			// Store file paths in an array
			$realFileArr[] = $server_target_file;
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
	}
}

if (isset($realFileArr)) {
	$mySQLfiles = ''.implode('|',$realFileArr);
} else {
	$mySQLfiles = '';
}

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// Prepare DB Create Statement
$sql = "CREATE DATABASE IF NOT EXISTS ar_tooling_inspection;";

// Check for DB, if it doesn't exist... Create it
if ($conn->query($sql) === TRUE) {
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname );
} else {
    echo "Error creating database: " . $conn->error;
}

// Prepare Table create statement
$sql = "CREATE TABLE IF NOT EXISTS midplane_inspection (
id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
serial varchar(25) NOT NULL,
assy_num varchar(25) NOT NULL,
asset_tag varchar(25) NOT NULL,
mac_addr varchar(25) NOT NULL,
user varchar(25) NOT NULL,
status varchar(10) NOT NULL,
badFM int(5) NOT NULL DEFAULT 0,
badLC int(5) NOT NULL DEFAULT 0,
cleaned varchar(5),
comments varchar(255),
pic_path varchar(2000),
date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)";

// Check for Table, if it doesn't exist... Create it
if ($conn->query($sql) === TRUE) {
} else {
    echo "Error creating table: " . $conn->error;
}


$sql = "INSERT INTO $tableName_inspections (serial, assy_num, asset_tag, mac_addr, user, status, badFM, badLC, cleaned, comments, pic_path)
				VALUES ('$serialNum','$assyNum ','$arAsset','$macAddress','$user','$passfail','$badFMs','$badLCs','$cleaned','$comment','$mySQLfiles');";
			if ($conn->query($sql) === TRUE) {
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

echo "r|r";

echo "Added log of $arAsset as $passfail to DB";

// Close Connection
$conn->close();
?>