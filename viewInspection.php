<?php
// Author: Troy Braswell, July 18, 2018

// Set Server Info
include('config.php');

// Set Logging
ini_set("error_log","C:\Apache24\logs\midPlane_error.log");
ini_set("display_errors","Off");

// Get Args
$q = $_POST['str'];
$date = explode ("x|x",$q)[0];
$asset = explode ("x|x",$q)[1];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

 // Prepare Statement
$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.pic_path,m.badLC,m.badFM,u.product
		FROM $tableName_inspections m
		LEFT JOIN $tableName_utds u ON m.serial = u.serial
		WHERE m.date='$date'
		AND m.asset_tag='$asset'";		

// Execute Statement
$result_info = $conn->query($sql);

// Process results into Array, if results were found
if ($result_info->num_rows > 0) {
	
	while($row = $result_info->fetch_assoc()) {
		$id			= $row['id'];
		$status 	= $row['status'];
		$assy_num 	= $row['assy_num'];
		$mac_addr 	= $row['mac_addr'];
		$user 		= $row['user'];
		$serial 	= $row['serial'];
		$comments 	= $row['comments'];
		$pic_path 	= $row['pic_path'];
		$cleaned	= $row['cleaned'];
		$badLC		= $row['badLC'];
		$badFM		= $row['badFM'];
		$product	= $row['product'];
		
	}
	$test['id'] =$id;
	$test['status'] =$status;
	$test['assy_num'] =$assy_num;
	$test['mac_addr'] =$mac_addr;
	$test['user'] =$user;
	$test['serial'] =$serial;
	$test['cleaned'] =$cleaned;
	$test['comments'] =$comments;
	$test['pic_path'] =$pic_path;
	$test['asset'] =$asset;
	$test['date'] =$date;
	$test['badLC'] =$badLC;
	$test['badFM'] =$badFM;
	$test['product'] =$product;
	
	print_r($row);
	$darkColor = '#00b3b3';
	$lightColor = '#FFFFDB';
	$failColor = '#ed2d1c';
	echo "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspection for\n$asset on $date</h2>";
	
	echo "<footer class='w3-container' style='width:  600px;'>";
	echo "<div class='grid-container' style='background-color: transparent;'>";
	echo "<div style='background-color: #00b3b3;'><a href='exportDRDB.php?arg1=indiv&arg2=$asset&arg3=$date&arg4=$pic_path'>Export Full Log       </a></div>";
	echo "<div style='background-color: #00b3b3;'><a href='javascript:void(0)' onclick='editLog(" . json_encode($test, JSON_PRETTY_PRINT) . ")'>Edit Log</a></div></div>";
	echo "</footer>";
	
	echo "<div class='w3-content w3-display-container' id='myslidediv' style='height: 20em;'>";
		foreach (explode ("|",$pic_path) as $pic) {
			if ($pic) {
				echo "<img class='mySlides' src='" . $pic . "' style='height:inherit'>";
			} else {
				echo "<img class='mySlides' src='assets/No_picture_available.png' style='height:inherit'>";
			}

		}
	  echo "<button class='w3-button w3-black w3-display-left' onclick='plusDivs(-1)'>&#10094;</button>";
	  echo "<button class='w3-button w3-black w3-display-right' onclick='plusDivs(1)'>&#10095;</button>";
	  
	echo "</div>";
	echo "<table style='font-size: medium;'>
	<tr style='background-color: $darkColor;'>
	<th>Date</th>
	<th>Status</th>
	<th>Cleaned</th>
	<th>Serial</th>
	<th>Product</th>
	<th>Assembly Number</th>
	<th>Asset Tag</th>
	<th>Mac Address</th>
	<th>Inspector</th>
	<th># Bad LC</th>
	<th># Bad FM</th>
	<th>Comments</th>
	</tr>";
	
	echo "<tr style='background-color: $lightColor;'><td style='text-align: center;border: 1px solid black ;'>" . $date . "</td>";
	if ($row['status'] == 'FAIL'){
		echo "<td style='text-align: center;border: 1px solid black ;background-color: $failColor;'><a href='javascript:void(0)' onclick='viewInspection(" . $row['date'] ."," . $row['asset_tag'] . ");'>" . $row['status'] . "</a></td>";
	} else {
		echo "<td style='text-align: center;border: 1px solid black ;'>" . $status . "</td>";
		
	}
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $cleaned . "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $serial . "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $product . "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $assy_num . "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $asset . "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $mac_addr . "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $user. "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $badLC. "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $badFM. "</td>";
	echo "<td style='text-align: center;border: 1px solid black ;'>" . $comments . "</td>";
		
			
	echo "</tr>";
		
	
	echo "</table>";
	
	echo "<script>\n";
			
			echo "showDivs('slideIndex');\n";
			echo "alert('exists');";
			
		echo "</script>\n";
	

} else {
	// If no Results were found
	echo "<br><b>No Results found in DB</b>";
}
// Close Connection
$conn->close();
?>