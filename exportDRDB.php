<?php
// Author: Troy Braswell, July 19, 2018


 // Set Logging
ini_set("error_log","C:\Apache24\logs\midPlane_error.log");
ini_set("display_errors","Off");
// Set Vars
include('config.php');

$search_type = $_GET['arg1'];

if ($search_type == 'date' or $search_type == 'date2'){
	$startDate = $_GET['arg2'];
	$endDate = $_GET['arg3'];
	$file = "MidPlane-Inspection_".$startDate."_".$endDate.".csv";
	// Prepare Statement	
	$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
		FROM $tableName_inspections m
		LEFT JOIN $tableName_utds u ON m.serial = u.serial
		WHERE m.date>='$startDate'
		AND m.date<='$endDate'
		ORDER BY m.date ASC"; 
	
} else if ($search_type == 'asset'){
	$asset = $_GET['arg2'];
	$file = "MidPlane-Inspection_".$asset.".csv";
	$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
		FROM $tableName_inspections m
		LEFT JOIN $tableName_utds u ON m.serial = u.serial
		WHERE m.asset_tag ='$asset'
		ORDER BY m.date ASC";

} else if ($search_type == 'indiv'){
	$asset = $_GET['arg2'];
	$date = $_GET['arg3'];
	$pic_path = $_GET['arg4'];
	$file = "MidPlane-Inspection_".$asset. "-" . explode (" ",$date)[0]  . ".csv";
	
	$zipfiles[] = $file;
	foreach (explode ("|",$pic_path) as $pic) {		// Remove part of path, so server will look locally (on server)
		$zipfiles[] = str_replace($serverURL . "/TestFixture_dailys/", "", "$pic");
	}

	$zipname = "MidPlane-Inspection_".$asset. "-" . explode (" ",$date)[0]  . ".zip";
	$zip = new ZipArchive;
	
	$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
		FROM $tableName_inspections m
		LEFT JOIN $tableName_utds u ON m.serial = u.serial
		WHERE m.asset_tag ='$asset'
		AND m.date='$date'
		ORDER BY m.date ASC";
	
} else if ($search_type == 'sql-30days-plus'){
	$name = $_GET['arg2'];
	$thirtyDaysAgo= $_GET['arg3'];
	$file = "MidPlane-Inspection_".$name.".csv";
	
	$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
	FROM $tableName_inspections m
	LEFT JOIN $tableName_utds u ON m.serial = u.serial
	WHERE m.date<='$thirtyDaysAgo'
	GROUP BY(m.serial)";
	
	error_log($sql, 0);
} else if ($search_type == 'sql-no_insp'){
	$name = $_GET['arg2'];
	$endDate= $_GET['arg3'];
	$file = "MidPlane-Inspection_".$name.".csv";
	$sql = "SELECT serial
	FROM $tableName_utds 
	WHERE serial NOT IN 
	(SELECT serial 
	FROM $tableName_inspections 
	WHERE date>=$endDate 
	GROUP BY(serial)) 
	GROUP BY(serial)";
	error_log($sql, 0);
}


$output = fopen($file,'w') or die("Can't open DBexport.csv");
$filepath = $file;



// Write Headers to csv
fputcsv($output, array('Id','Serial Number','Assembly Number','Asset Tag','Mac Address','User','Status','Cleaned','Comments','Date','Number of Bad FM slots','Number of Bad LC slots', 'Product'));

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
		
// Execute Statement
$result = $conn->query($sql);
// If results were found process data and DL
if ($result->num_rows > 0) {
	header('Content-Description: File Transfer');
	header("Content-Type:application/csv"); 
	header("Content-Disposition:attachment;filename=pressurecsv.csv"); 
	while($row = $result->fetch_assoc()) {
		fputcsv($output, $row);
	}
	fclose($output) or die("Can't close LB_cleaningDB.csv"); 
	// Process download
    if(file_exists($filepath)) {
        header('Content-Description: File Transfer');
		if ($search_type == 'indiv'){
			
			if ($zip->open($zipname, ZipArchive::CREATE)!==TRUE) {
				error_log("cannot open zip",0);
			  exit("cannot open <$zipname>\n");
			 }
			
			$zip->open($zipname, ZipArchive::CREATE);
			foreach ($zipfiles as $file) {
				if (file_exists($file)) {
					error_log("file exists",0);
				} else {
					error_log("file does not exist",0);
				}
			  $zip->addFile($file,basename($file));
			}
			$zip->close();
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zipname));
			flush();
			readfile($zipname);
			unlink($zipname);
			
		} else {
			header('Content-Type: application/force-download');
			header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($filepath));
			flush(); // Flush system output buffer
			readfile($filepath);
		}
		unlink($filepath);
		
        exit;
    }
	
} else {
	// If no Results were found
	echo "<br><b>No Results found in DB</b>";
} 

//Close Connection
$conn->close(); 
?>
