<?php
// Author: Troy Braswell, Oct. 26, 2018

// Set Server Info
include('config.php');

$search_type = 'date2';

// Set Logging
ini_set("error_log","C:\Apache24\logs\midPlane_error.log");
ini_set("display_errors","Off");

// Get Args
$q = $_POST['str'];

$date = $q;
$datetime_start = new DateTime($date);



// Offset End Date by 1 day for search
$endDate = date("Y-m-d", strtotime($date. ' + 1 days'));
$sql = "SELECT serial, assy_num, asset_tag, mac_addr,user,status,comments,date
	FROM $tableName_inspections
	WHERE date>='$date'
	GROUP BY(serial)";
$title = "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections\n$date</h2>";
$exportBtn = "<a href='exportDRDB.php?arg1=$search_type&arg2=$date&arg3=$endDate'>Export Results</a>";
$str = "'" . $date . 'x|x' . $endDate . "'";
$name = 'today';
$dataArray[] = [$endDate,$sql,$title,$exportBtn,$name,$search_type,$str];

// Units inspectedin the last 10 days
$endDate = date("Y-m-d", strtotime($date. ' - 10 days'));
// increment today by 1 so that sql will actually catch everything
$startDate = date("Y-m-d", strtotime($date. ' + 1 days'));
$sql = "SELECT serial, assy_num, asset_tag, mac_addr,user,status,comments,date
	FROM $tableName_inspections
	WHERE date>='$endDate'
	AND date<='$startDate'
	GROUP BY(serial)";
$title = "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections\n$endDate / $date</h2>";
$exportBtn = "<a href='exportDRDB.php?arg1=$search_type&arg2=$endDate&arg3=$startDate'>Export Results</a>";
$str = "'" . $endDate . 'x|x' . $startDate . "'";
$name = '10days';
$dataArray[] = [$endDate,$sql,$title,$exportBtn,$name,$search_type,$str];

// Units inspected in the last 30 days
$endDate = date("Y-m-d", strtotime($date. ' - 30 days'));
$sql = "SELECT serial, assy_num, asset_tag, mac_addr,user,status,comments,date
	FROM $tableName_inspections
	WHERE date>='$endDate'
	AND date<='$startDate'
	GROUP BY(serial)";
$title = "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections\n$endDate / $date</h2>";
$exportBtn = "<a href='exportDRDB.php?arg1=$search_type&arg2=$endDate&arg3=$startDate'>Export Results</a>";
$str = "'" . $endDate . 'x|x' . $startDate . "'";
$name = '30days';
$dataArray[] = [$endDate,$sql,$title,$exportBtn,$name,$search_type,$str];

// Units not inspected more than 30 days
$endDate = '2018-01-01';
$startDate = date("Y-m-d", strtotime($date. ' - 30 days'));

$sql = "SELECT m.id,m.serial, m.assy_num, m.asset_tag, m.mac_addr,m.user,m.status,m.cleaned,m.comments,m.date,m.badLC,m.badFM,u.product
	FROM $tableName_inspections m
	LEFT JOIN $tableName_utds u ON m.serial = u.serial
	WHERE m.date<='$startDate'
	GROUP BY(m.serial)";
$title = "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections\n$endDate / $startDate </h2>";
$search_type_new = 'sql-30days-plus';
$fileName = 'Not-insp-30days+';
$exportBtn = "<a href='exportDRDB.php?arg1=$search_type_new&arg2=$fileName&arg3=$startDate'>Export Results</a>";

$name = '30daysplus';
$str = "'" . $name . 'x|x' . $startDate . "'";
$dataArray[] = [$endDate,$sql,$title,$exportBtn,$name,$search_type_new,$str];

// Units never inspected
$endDate = '2018-01-01';
$sql = "SELECT serial
	FROM $tableName_utds 
	WHERE serial NOT IN 
	(SELECT serial 
	FROM $tableName_inspections 
	WHERE date>='$endDate'
	GROUP BY(serial)) 
	GROUP BY(serial)";
$title = "<h2 style='background-color: #87b987;border-style: solid;width:  600px;'>MidPlane Inspections\n$endDate / $startDate </h2>";
$search_type_new = 'sql-no_insp';
$fileName = 'Never_inspected';
$exportBtn = "<a href='exportDRDB.php?arg1=$search_type_new&arg2=$fileName&arg3=$endDate'>Export Results</a>";
$name = 'neverinsp';
$str = "'" . $name . 'x|x' . $endDate . "'";
$dataArray[] = [$endDate,$sql,$title,$exportBtn,$name,$search_type_new,$str];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

foreach ($dataArray as $sql_query) {
	$endDate 	= $sql_query[0];
	$sql 		= $sql_query[1];
	$title 		= $sql_query[2];
	$exportBtn 	= $sql_query[3];
	$name 		= $sql_query[4];
	$searchType = "'" . $sql_query[5] . "'";
	$str		= $sql_query[6];
	$datetime_end = new DateTime($endDate);

	if ($datetime_start->diff($datetime_end)->days == 1) {
		$dateRangeText 	= '# of Units Inspected Today';
		$numberURL		= '<a href="javascript:void(0)" onclick="dateRange('.$searchType . ',' . htmlspecialchars($str, ENT_QUOTES).')">View Inspections</a>';
	} elseif ($datetime_start->diff($datetime_end)->days == 10) {
		$dateRangeText = '# of Units Inspected in the last 10 days';
		$numberURL		= '<a href="javascript:void(0)" onclick="dateRange('. $searchType .',' . htmlspecialchars($str, ENT_QUOTES).')">View Inspections</a>';
	} elseif ($datetime_start->diff($datetime_end)->days == 30) {
		$dateRangeText = '# of Units Inspected in the last 30 days';
		$numberURL		= '<a href="javascript:void(0)" onclick="dateRange('. $searchType .',' . htmlspecialchars($str, ENT_QUOTES).')">View Inspections</a>';
	} elseif ($datetime_start->diff($datetime_end)->days > 30 && $name != 'neverinsp') {
		$dateRangeText = '# of Units Inspected more than 30 days ago';
		$numberURL		= '<a href="javascript:void(0)" onclick="dateRange('. $searchType .',' . htmlspecialchars($str, ENT_QUOTES).')">View Inspections</a>';
	} elseif ($datetime_start->diff($datetime_end)->days > 30 && $name == 'neverinsp') {
		$dateRangeText = '# of Units Never Inspected';
		$numberURL		= '<a href="javascript:void(0)" onclick="dateRange('. $searchType .',' . htmlspecialchars($str, ENT_QUOTES).')">View Inspections</a>';
	}
	
	
	// Execute Statement
	$result_info = $conn->query($sql);
	
	// Get Number of units
	
	if ($result_info->num_rows > 0) {
		$numRows = $result_info->num_rows;
		echo "<div class='w3-container'>";

		  echo "<div class='w3-card-4' style='width:50%;'>";
			echo "<header class='w3-container w3-blue'>";
			echo "<h1>$dateRangeText</h1>";
			echo "</header>";

			echo "<div class='w3-container'>";
			  echo "<p>$numRows</p>";
			echo "</div>";

			echo "<footer class='w3-container w3-blue'>";
				echo "<div class='grid-container'>";
					echo "<div>$numberURL</div>";
					echo "<div>$exportBtn</div>";
			echo "</footer>";
		  echo "</div>";
		echo "</div>";
		
		echo "<br>";
	} else {
		echo "<div class='w3-container'>";

		  echo "<div class='w3-card-4' style='width:50%;'>";
			echo "<header class='w3-container w3-blue'>";
			echo "<h1>$dateRangeText</h1>";
			echo "</header>";

			echo "<div class='w3-container'>";
			  echo "<p>0</p>";
			echo "</div>";

			echo "<footer class='w3-container w3-blue'>";
				echo "<div class='grid-container'>";
					echo "<div>$numberURL</div>";
					echo "<div>$exportBtn</div>";
			echo "</footer>";
		  echo "</div>";
		echo "</div>";
		
		echo "<br>";
	} 

}
// Close Connection
$conn->close();
?>