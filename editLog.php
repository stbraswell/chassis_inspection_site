<?php
// Author: Troy Braswell, March 13, 2018
ini_set("error_log","C:\Apache24\logs\editLog.log");
ini_set("display_errors","Off");


// Grab data passed from html
if (isset($_POST['json_string'])) {
	$q = json_decode( $_POST['json_string'], true );
	error_log("test");
	
	$id			= $q['id'];
	$status		= $q['status'];
	$assy_num	= $q['assy_num'];
	$mac_addr	= $q['mac_addr'];
	$user		= $q['user'];
	$serial		= $q['serial'];
	$cleaned	= $q['cleaned'];
	$comments	= $q['comments'];
	$pic_path	= $q['pic_path'];
	$asset		= $q['asset'];
	$date		= $q['date'];
	$badLC		= $q['badLC'];
	$badFM		= $q['badFM'];

}

$serverURL = 'http://sjc1amwk518277';
$target_dir = "TestFixture_dailys/uploads/";

if (isset($_FILES["files"])) {
	$numberOfFiles = sizeof($_FILES["files"]["name"]);
} else {
	$numberOfFiles = 0;
}
$uploadOk = 1;

// Setup Vars
include('config.php');

// Set Logging
ini_set("error_log","C:\Apache24\logs\midPlane_error.log");
ini_set("display_errors","Off");


echo '<h2 style="background-color: #87b987;border-style: solid;width:  600px;">Edit Inspection Log</h2>';

echo '<div id="top" align="middle"></div>';

echo '<div align="middle" style="width: 600px;text-align: left;overflow: hidden;">';
echo '<div id="passfailslider" align="middle">';
	echo '<label class="passswitch">';
		if ($status == "PASS") {
			echo '<input type="checkbox" id="passSwitch" checked>';
		} else {
			echo '<input type="checkbox" id="passSwitch">';
		}
	  echo '<span class="passslider round"></span>';
	echo '</label>';
echo '</div>';
echo '<form action="">';
  echo '<label for="cleaned" >Cleaning Performed: </label>';
  if ($cleaned == "X") {
	echo '<input type="checkbox" name="cleaned" id="cleaned_yes" value="yes" checked="checked">';
  } else {
	echo '<input type="checkbox" name="cleaned" id="cleaned_yes" value="yes">';
  }
echo '</form>';
echo '<p class="formfield">';
echo '<label for="user_name" >User Name: </label>';
echo "<textarea class='readonlyclass' name='user_name' id='user_name' cols='40' rows='1' autofocus style='float: right;resize: none;' readonly>$user</textarea>";
echo '</p>';
echo '<p class="formfield">';
echo '<label for="arista_asset">Arista Tag #: </label>';
echo "<textarea class='readonlyclass' name='arista_asset' id='arista_asset' cols='40' rows='1' autofocus style='float: right;resize: none;' readonly>$asset</textarea>";
echo '</p>';
echo '<p class="formfield">';
echo '<label for="mac_address">MAC Address: </label>';
echo "<textarea class='readonlyclass' name='mac_address' id='mac_address' cols='40' rows='1' autofocus style='float: right;resize: none;' readonly>$mac_addr</textarea>";
echo '</p>';
echo '<p class="formfield">';
echo '<label for="assembly_number">Assembly number: </label>';
echo "<textarea class='readonlyclass' name='assembly_number' id='assembly_number' cols='40' rows='1' autofocus style='float: right;resize: none;' readonly>$assy_num</textarea>";
echo '</p>';
echo '<p class="formfield">';
echo '<label for="serial_number">Serial number: </label>';
echo "<textarea class='readonlyclass' name='serial_number' id='serial_number' cols='40' rows='1' autofocus style='float: right;resize: none;' readonly>$serial</textarea>";
echo '</p>';
echo '<p class="formfield">';
echo '<label for="bad_fm">Number of Bad FM Slots: </label>';
echo "<textarea name='bad_fm' id='bad_fm' cols='40' rows='1' autofocus style='float: right;resize: none;'>$badFM</textarea>";
echo '</p>';
echo '<p class="formfield">';
echo '<label for="bad_lc">Number of Bad LC Slots: </label>';
echo "<textarea name='bad_lc' id='bad_lc' cols='40' rows='1' autofocus style='float: right;resize: none;'>$badLC</textarea>";
echo '</p>';
echo '<p class="formfield">';
echo '<label for="comment">Comments: </label>';
echo "<textarea name='comment' id='comment' cols='40' rows='10' autofocus style='float: right;resize: none;'>$comments</textarea>";
echo '</p>';
echo '<form>';
  echo 'Select files: <input type="file" name="myFile[]" id="myfile" multiple="multiple"><br><br>';
 echo '<!--  <input type="submit"> -->';
echo '</form>';
echo '</div>';
echo '<br>';

echo "<button id='oneLinerPrint' onclick='add_edit_2DB(&#39;$date&#39;&#44;&#39;$id&#39;)' style='display:block'>Add to DB</button>";


echo "<!-- AutoEnter from scanner -->";
echo '<script>';
	echo "$('input[type=\"checkbox\"]').click(function(e){";
   echo "e.preventDefault();";
echo "});";
	echo "$('#Text1').focus();";
	echo "$('#Text1').keypress(function (e) {";
	echo "var check=$('#inputSwitch').is(':checked');";
	echo "if(e.which == 13 && !e.shiftKey) {";
		echo "if (check == true){";
			echo "add_edit_2DB(&#39;$date&#39;);";
			echo "e.preventDefault();";
			echo "return false;";
			echo "}";
	echo "}";
	echo "});";
	
echo "</script>";


?>